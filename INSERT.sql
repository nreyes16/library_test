USE [library_test]
GO

CREATE SCHEMA [cat]
GO

CREATE SCHEMA [tbl]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [cat].[categoria](
	[id_cat] [int] NOT NULL,
	[categoria] [varchar] (100) NOT NULL,
	CONSTRAINT [PK_Categoria] PRIMARY KEY CLUSTERED
	(
	[id_cat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [tbl].[libro](
	[id_lib] [int] NOT NULL,
	[nombre] [varchar] (100) NOT NULL,
	[id_cat] [int] NOT NULL,
	[id_autor] [int] NOT NULL
	CONSTRAINT [PK_Libro] PRIMARY KEY CLUSTERED
	(
	[id_lib] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [tbl].[autor](
	[id_aut] [int] NOT NULL,
	[nombre] [varchar] (100) NOT NULL,
	CONSTRAINT [PK_autor] PRIMARY KEY CLUSTERED
	(
	[id_aut] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

SET ANSI_PADDING OFF
GO

INSERT [cat].[categoria](id_cat,categoria) VALUES(1,'History')
INSERT [cat].[categoria](id_cat,categoria) VALUES(2,'Biology')
INSERT [cat].[categoria](id_cat,categoria) VALUES(3,'Information Technologies')
INSERT [cat].[categoria](id_cat,categoria) VALUES(4,'Geography')
INSERT [cat].[categoria](id_cat,categoria) VALUES(5,'Literature')
INSERT [tbl].[autor](id_aut,nombre) VALUES(1,'William Shakespare')
INSERT [tbl].[autor](id_aut,nombre) VALUES(2,'Charles Dickens')
INSERT [tbl].[autor](id_aut,nombre) VALUES(3,'Charles Marx')
INSERT [tbl].[autor](id_aut,nombre) VALUES(4,'Charles Darwin')
INSERT [tbl].[libro](id_lib,nombre,id_cat,id_autor) VALUES(1,'Romero y Julieta',5,1)
INSERT [tbl].[libro](id_lib,nombre,id_cat,id_autor) VALUES(2,'Moby Dick',5,2)
INSERT [tbl].[libro](id_lib,nombre,id_cat,id_autor) VALUES(3,'Historia',1,3)
INSERT [tbl].[libro](id_lib,nombre,id_cat,id_autor) VALUES(4,'Marxismo',1,3)
INSERT [tbl].[libro](id_lib,nombre,id_cat,id_autor) VALUES(5,'Evolución',2,4)