﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default2.aspx.vb" Inherits="Default2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Library Test</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Welcome to the Library Test</h1>
        <p>This are the List of Books available on the library.</p>
        <p>
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" DataKeyNames="id_lib" DataSourceID="library_test" ForeColor="Black" GridLines="None">
                <AlternatingRowStyle BackColor="PaleGoldenrod" />
                <Columns>
                    <asp:BoundField DataField="id_lib" HeaderText="id_lib" ReadOnly="True" SortExpression="id_lib" />
                    <asp:BoundField DataField="nombre" HeaderText="nombre" SortExpression="nombre" />
                    <asp:BoundField DataField="categoria" HeaderText="categoria" SortExpression="categoria" />
                    <asp:BoundField DataField="autor" HeaderText="autor" SortExpression="autor" />
                </Columns>
                <FooterStyle BackColor="Tan" />
                <HeaderStyle BackColor="Tan" Font-Bold="True" />
                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                <SortedAscendingCellStyle BackColor="#FAFAE7" />
                <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                <SortedDescendingCellStyle BackColor="#E1DB9C" />
                <SortedDescendingHeaderStyle BackColor="#C2A47B" />
            </asp:GridView>
            <asp:SqlDataSource ID="library_test" runat="server" ConnectionString="<%$ ConnectionStrings:library_testConnectionString %>" SelectCommand="SELECT tbl.libro.id_lib, tbl.libro.nombre, cat.categoria.categoria, tbl.autor.nombre AS autor FROM tbl.libro INNER JOIN cat.categoria ON tbl.libro.id_cat = cat.categoria.id_cat INNER JOIN tbl.autor ON tbl.libro.id_autor = tbl.autor.id_aut"></asp:SqlDataSource>
        </p>
    </div>
    </form>
    <p>
        Made by Norma Reyes</p>
</body>
</html>
